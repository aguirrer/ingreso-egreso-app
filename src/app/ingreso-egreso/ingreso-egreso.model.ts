export class IngresoEgreso {
  public id: number;
  public description: string;
  public mount: number;
  public income_expenditure_type_id: number;

  constructor( obj: DataObj ) {
    this.id = obj && obj.id || null;
    this.description = obj && obj.description || null;
    this.mount = obj && obj.mount || null;
    this.income_expenditure_type_id = obj && obj.income_expenditure_type_id || null;
  }
}

interface DataObj {
  id: number;
  description: string;
  mount: number;
  income_expenditure_type_id: number;
}
