import { Injectable } from '@angular/core';
import { IngresoEgreso } from './ingreso-egreso.model';
import { AuthService } from '../auth/auth.service';

import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { filter, map } from 'rxjs/operators';
import { SetItemsAction, UnsetItemsAction } from './ingreso-egreso.actions';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Dictionary } from '../shared/dictionary.interface';

@Injectable({
  providedIn: 'root'
})
export class IngresoEgresoService {
  ingresoEgresoItemsSubscription: Subscription = new Subscription();
  private api = `${environment.apiUrl}/api/v1`;

  constructor( private authService: AuthService,
              private store: Store<AppState>,
              private http: HttpClient ) { }

  loadIncomeExpenditures() {
    const url = `${this.api}/income_expenditures`;
    this.ingresoEgresoItemsSubscription = this.http.get<Dictionary>( url ).subscribe( (data: Dictionary) => {
      const accion = new SetItemsAction( data['income_expenditures'] );
      this.store.dispatch( accion );
    });
  }

  cancelarSubscriptions() {
    this.ingresoEgresoItemsSubscription.unsubscribe();
    const accion = new UnsetItemsAction();
    this.store.dispatch( accion );
  }

  createIncomeExpenditure( ingresoEgreso: IngresoEgreso ) {
    const data = {
      income_expenditure: {
        ...ingresoEgreso
      }
    };
    const url = `${this.api}/income_expenditures`;
    return this.http.post<Dictionary>( url, data );
  }

  deleteIncomeExpenditures( id: number ) {
    const url = `${this.api}/income_expenditures/${id}`;
    return this.http.delete<Dictionary>( url );
  }
}
