export class User {
  public name: string;
  public last_name: string;
  public uid: string;
  public email: string;
  public username: string;

  constructor( obj: DataObj ) {
    this.name = obj && obj.name || null;
    this.last_name = obj && obj.last_name || null;
    this.uid = obj && obj.uid || null;
    this.email = obj && obj.email || null;
    this.username = obj && obj.username || null;
  }
}

interface DataObj {
  name: string;
  last_name: string;
  uid: string;
  email: string;
  username: string;
}
