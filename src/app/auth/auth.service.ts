import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, filter, take, last, shareReplay } from 'rxjs/operators';

import { User } from './user.model';

import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { ActivarLoadingAction, DesactivarLoadingAction } from '../shared/ui.actions';
import { UnsetUserAction, SetUserAction } from './auth.actions';
import { environment } from '../../environments/environment';

import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Dictionary } from '../shared/dictionary.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubscription: Subscription = new Subscription();
  private api = `${environment.apiUrl}/api/v1`;
  private isAuth: boolean;

  constructor( private http: HttpClient, private router: Router, private store: Store<AppState> ) { }

  crearUsuario( name, last_name, username, email, password ) {
    const data = {
      user: {
        name,
        last_name,
        username,
        email,
        password
      }
    };
    const activarAccion = new ActivarLoadingAction();
    this.store.dispatch( activarAccion );

    const url = `${this.api}/signup`;
    return this.http.post<Dictionary>( url, data );
  }

  login( correo, password ) {
    const data = {
      session: {
        email: correo,
        password
      }
    };
    const activarAccion = new ActivarLoadingAction();
    this.store.dispatch( activarAccion );

    const url = `${this.api}/login`;
    return this.http.post<Dictionary>( url, data ).pipe(shareReplay(1));
  }

  logout() {
    const accion = new UnsetUserAction();
    this.store.dispatch( accion );
    this.router.navigate(['/login']);
  }

  getUser() {
    const url = `${this.api}/users`;
      return this.http.get<Dictionary>(url).pipe(map( (data: Dictionary) => {
        if (data['user'] !== null) {
          const accion = new SetUserAction(data['user'], localStorage.getItem('tkn'));
          this.store.dispatch( accion );
        }
        return data['user'] !== null;
      }));
  }

  getToken() {
    return localStorage.getItem('tkn');
  }
}
