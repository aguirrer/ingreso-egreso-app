import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { AuthService } from './auth.service';
import { take } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanLoad {

  constructor( private authService: AuthService, private store: Store<AppState> ) { }

  canLoad() {
    return this.authService.getUser().pipe(take(1));
  }
}
