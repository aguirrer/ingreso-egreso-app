

import * as fromAuth from './auth.actions';
import { User } from './user.model';

export interface AuthState {
  user: User;
  token: string;
}

const initState: AuthState = {
  user: null,
  token: null
};

export function authReducer( state = initState, action: fromAuth.Acciones ): AuthState {
  switch ( action.type ) {
    case fromAuth.SET_USER:
      const { user, token } = action;
      return {
        user: { ...user },
        token
      };
    case fromAuth.UNSET_USER:
      return {
        user: null,
        token: null
      };
    default:
      return state;
  }
}
