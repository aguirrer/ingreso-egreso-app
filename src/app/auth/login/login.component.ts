import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import Swal from 'sweetalert2';

import { AuthService } from '../auth.service';
import { AppState } from 'src/app/app.reducer';
import { Dictionary } from 'src/app/shared/dictionary.interface';
import { DesactivarLoadingAction } from 'src/app/shared/ui.actions';
import { SetUserAction } from '../auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  cargando: boolean;
  subscription: Subscription = new Subscription();

  constructor(
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router
  ) { }

  ngOnInit() {
    this.subscription = this.store.select('ui').subscribe(ui => {
      this.cargando = ui.isLoading;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSubmit( form: NgForm ) {
    const data = form.value;
    const desactivarAccion = new DesactivarLoadingAction();

    this.authService.login( data.correo, data.password ).subscribe( (resp: Dictionary) => {
      const token = resp['auth_token'];
      localStorage.setItem( 'tkn', token );
      localStorage.setItem( 'tkn_exp', resp['expires_at'] );
      const setUserAccion = new SetUserAction( resp['user'], token );
      this.store.dispatch( setUserAccion );
      this.store.dispatch( desactivarAccion );
      this.router.navigate(['/']);
    }, (error: HttpErrorResponse) => {
      this.store.dispatch( desactivarAccion );
      Swal('Error en el login', '', 'error');
    });
  }
}
