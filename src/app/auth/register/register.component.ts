import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import Swal from 'sweetalert2';

import { AuthService } from '../auth.service';
import { AppState } from 'src/app/app.reducer';
import { Dictionary } from 'src/app/shared/dictionary.interface';
import { DesactivarLoadingAction } from 'src/app/shared/ui.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  cargando: boolean;
  subscription: Subscription = new Subscription();

  constructor( private authService: AuthService,
              private store: Store<AppState> ) { }

  ngOnInit() {
    this.subscription = this.store.select('ui').subscribe(ui => {
      this.cargando = ui.isLoading;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSubmit( form: NgForm ) {
    const data = form.value;
    const desactivarAccion = new DesactivarLoadingAction();
    this.authService.crearUsuario( data.name, data.last_name, data.username, data.email, data.password ).subscribe(
      (res: Dictionary) => {
        this.store.dispatch(desactivarAccion);
        form.resetForm();
        Swal('Registro exitoso', '', 'success');
      },
      (error: HttpErrorResponse) => {
        this.store.dispatch(desactivarAccion);
        Swal('Error en el registro', '', 'error');
      }
    );
  }
}
